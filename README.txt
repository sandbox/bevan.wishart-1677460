= Experimental Project =

This is a sandbox project, which contains experimental code for developer use
only.

= What =

Creates a /cron/last menu path which outputs simple text (no HTML) status of
cron.

Example output should have not run in the specified time.
FAIL - cron was last run at Tue, 10/07/2012 - 13:23 - [1150] seconds ago

Example output if cron has run within specified time.
OK

= Installation and setup =

Check the version control for instructions on how to clone the repository, be
sure to select the relevant branch to work from, from the dropdown.

* For Drupal 7 checkout the 7.x-1.x branch
* For Drupal 6 checkout the 6.x-1.x branch

Once you have cloned the repo, copy the module files to
<your drupal site>/sites/all/modules/custom/

enable the module with
 drush en -y cron_last

and access the cron status at http://<your_drupal_domain>/cron/last

By default this module will check if cron has run within the last 900 seconds
(15 minutes).
You can override this threshold by setting the cron_last_sec_threshold drupal
variable

eg.
drush vset -y cron_last_sec_threshold 1800
